# a1x + b1y = c1
# a2x + b2y = c2
def solve(a1, b1, c1, a2, b2, c2)
  x = (c1*b2 - c2*b1) / (a1*b2 -a2*b1)
  x = 0.0 if x == -0.0
  y = (a1*c2 - a2*c1) / (a1*b2 -a2*b1)
  y = 0.0 if y == -0.0
  return x, y
end

$<.each do |l|
  a, b, c, d, e, f = l.split.map(&:to_f)
  x, y = solve(a, b, c, d, e, f)
  printf("%.3f %.3f\n", x, y)
end
