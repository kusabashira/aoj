$<.each do |l|
  n = l.to_i
 
  noCombination = 0
  (0..9).to_a.repeated_permutation(4) do |c|
    noCombination += 1 if c.inject(:+) == n
  end
  puts noCombination
end
